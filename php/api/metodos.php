<?php

require_once '../model/CandidatoModel.php';
require_once '../model/CodigosModel.php';

if(isset($_POST))
{
    $jsonInput=file_get_contents('php://input');
    
    if(!empty($jsonInput)){
        
        $data= json_decode($jsonInput);
        
        //Request viene del formulario en el modal
        if(isset($data->from_modal))
        {
            $bd=new CandidatoModel();
            $bd->agregarCandidatoModal($jsonInput);
        }
      
        //Vienen del formulario datos completos
        else{   
             $bd = new CandidatoModel();
             $bd->agregarCandidato($jsonInput);
        }
    }
    
         
    else{
        if(isset($_POST["tipo"])){
            
            if($_POST["tipo"]=="getCodigos"){
                $codigos=new CodigosModel();
                echo $codigos->getCps();
            }
            else if($_POST["tipo"]=="getData"){
                $cp=$_POST["cp"];
                $codigos=new CodigosModel();
                echo $codigos->getData($cp);
            }
            else if($_POST["tipo"]=="getDelegaciones"){
                $codigos=new CodigosModel();
                echo $codigos->getDelegaciones();
            }
            else if($_POST["tipo"]=="getColonias"){
                $codigos=new CodigosModel();
                echo $codigos->getColonias();
            }
        }
    }
}
