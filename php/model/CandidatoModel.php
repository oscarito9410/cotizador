<?php
require '../db/Conexion.php';
class CandidatoModel{
      private $conn;
     public function __construct() {
             $conexion=new Conexion();
             $this->conn=$conexion->getConexion();
     }
     public function getCandidatos(){
       
          $query=$this->conn->prepare("SELECT "
                  . "candidato_web.candidato_id,candidato_web.nombre, "
                  . "candidato_web.apellido_paterno, "
                  . "candidato_web.apellido_materno, "
                  . "candidato_web.telefono, "
                  . "candidato_web_datos_adicionales.telefono_fijo, "
                  . "candidato_web_datos_adicionales.telefono_movil, "
                  . "candidato_web_datos_adicionales.edad, "
                  . "candidato_web_datos_adicionales.colonia, "
                  . "candidato_web_datos_adicionales.colonia, "
                  . "candidato_web_datos_adicionales.delegacion, "
                  . "candidato_web_datos_adicionales.num_ext "
                  . "FROM candidato_web, candidato_web_datos_adicionales "
                  . "WHERE candidato_web.candidato_id=candidato_web_datos_adicionales.candidato_id ");
          $query->execute();
          $resultados=$query->fetchAll(PDO::FETCH_ASSOC);
          return json_encode($resultados);
       
     
              
     }
    
     public  function agregarCandidatoModal($json){
          if(!empty($json))
         {
             try
             {  
                $data=json_decode($json);
                 //AGREGAMOS LOS DATOS COMUNES DEL CANDIDATO
                $query=$this->conn->prepare("INSERT INTO 
                candidato_web(nombre,apellido_paterno,apellido_materno,telefono)
                VALUES(:nombre,:paterno,:materno,:telFijo)");
                $query->bindParam(":nombre", $data->nombre);
                $query->bindParam(":paterno",$data->paterno);
                $query->bindParam(":materno", $data->materno);
                $query->bindParam(":telFijo", $data->telFijo);
                $query->execute();
                $candidatoId=$this->getLastId();         
                echo '<p>TU SOLICITUD HA SIDO PROCESADA CORRECTAMENTE SU NÚMERO DE REGISTRO ES: <strong>'.$candidatoId. '</strong>  EN BREVE NOS COMUNICAREMOS CON USTED. </p>';
                 
             } catch (Exception $ex) {
                    echo 'ERROR'. $ex->getMessage();
             }
         }
     }
     //OBTENEMOS EL ID DEL CANDIDATO HACIENDO UN SELECT MAX 
     private function  getLastId(){
          $queryMax=$this->conn->prepare("SELECT MAX(candidato_id) FROM candidato_web");
          $queryMax->execute();
          $candidatoId=$queryMax->fetchColumn();
          return $candidatoId;
     }

     public function agregarCandidato($json){
         
         if(!empty($json))
         {
             try
             {
              
                $data=json_decode($json);
             
                
                //AGREGAMOS LOS DATOS COMUNES DEL CANDIDATO
                $query=$this->conn->prepare("INSERT INTO 
                candidato_web(nombre,segundo_nombre,apellido_paterno,apellido_materno,telefono)
                VALUES(:nombre,:segundo_nombre,:paterno,:materno,:telFijo)");
                $query->bindParam(":nombre", $data->nombre);
                $query->bindParam(":segundo_nombre",$data->segundo_nombre);
                $query->bindParam(":paterno",$data->paterno);
                $query->bindParam(":materno", $data->materno);
                $query->bindParam(":telFijo", $data->telFijo);
                $query->execute();
                
               

                $queryAdicionales=$this->conn->prepare("INSERT INTO candidato_web_datos_adicionales
                        (candidato_id,telefono_fijo,
                         telefono_movil,estado,
                         calle,num_ext,
                         num_int,colonia,
                         delegacion,cp,
                         giro_empresa,nombre_empresa,
                         nombre_empresa_ref1,nombre_empresa_ref2,
                         telefono_empresa_ref1,telefono_empresa_ref2,
                         nombre_ref1,telefono_ref1,
                         nombre_ref2,telefono_ref2,
                         edad,comprobante_domicilio,
                         identificacion_vigente,comprobante_ingresos,ingresos,
                         fecha_nacimiento,
                         sexo,email,longitud,laltitud)
                  VALUES(:candidato_id,:telFijo,
                         :telMovil,:estado,
                         :calle,:num_ext,
                         :num_int,:colonia,
                         :delegacion,:cp,
                         :giro_empresa,:nombre_empresa,
                         :referencia_empresa,:referencia2_empresa,
                         :ref1tel_empresa,:ref2tel_empresa,
                         :nombre_ref1,:telefono_ref1,
                         :nombre_ref2,:telefono_ref2,
                         :edad,:domicilio,
                         :identificacion,:comprobante_ingresos,
                         :ingresos,:fecha_nacimiento,
                         :sexo,:email,
                         :longitud,:laltitud
                         );");
               
                    $candidatoId=$this->getLastId();
                    $queryAdicionales->bindParam(":candidato_id", $candidatoId);
                    $queryAdicionales->bindParam(":telFijo", $data->telFijo);
                    $queryAdicionales->bindParam(":telMovil",$data->telMovil);
                    $queryAdicionales->bindParam(":estado", $data->estado);
                    $queryAdicionales->bindParam(":calle", $data->calle);
                    $queryAdicionales->bindParam(":num_ext",$data->exterior);
                    $queryAdicionales->bindParam(":num_int",$data->interior);
                    $queryAdicionales->bindParam(":colonia", $data->colonia);
                    $queryAdicionales->bindParam(":delegacion",$data->delegacion);
                    $queryAdicionales->bindParam(":cp", $data->cp);
                    $queryAdicionales->bindParam(":giro_empresa", $data->giro_empresa);
                    $queryAdicionales->bindParam(":nombre_empresa",$data->nombre_empresa);
                    $queryAdicionales->bindParam("referencia_empresa",$data->referencia_empresa);
                    $queryAdicionales->bindParam("referencia2_empresa",$data->referencia2_empresa);
                    $queryAdicionales->bindParam(":ref1tel_empresa", $data->ref1Tel_empresa);
                    $queryAdicionales->bindParam(":ref2tel_empresa", $data->ref2Tel_empresa);
                    $queryAdicionales->bindParam(":nombre_ref1",$data->nombre_ref1);
                    $queryAdicionales->bindParam(":telefono_ref1",$data->telefono_ref1);
                    $queryAdicionales->bindParam(":nombre_ref2",$data->nombre_ref2);
                    $queryAdicionales->bindParam(":telefono_ref2",$data->telefono_ref2);
                    $queryAdicionales->bindParam(":edad", $data->edad);
                    $queryAdicionales->bindParam(":sexo", $data->sexo);
                    $queryAdicionales->bindParam(":email", $data->email);
                    
                    //LALTITUD Y LONGITUD OBTENIDAS DESDE GOOGLE MAPS
                    $queryAdicionales->bindParam(":longitud",$data->longitud);
                    $queryAdicionales->bindParam(":laltitud",$data->laltitud);
                    
                    //PREPARAMOS DATOS ANTES DE ENVIAR
                    $domicilio=  CustomHelpers::bool2Int($data->domicilio);
                    $identificacion=  CustomHelpers::bool2Int($data->identificacion);
                    $comprobanteIngreso=  CustomHelpers::bool2Int($data->comprobante_ingresos);
                    $fechaNacimiento= CustomHelpers::getFecha($data->fecha_nacimiento);
                    $ingresos=  CustomHelpers::null2Int($data->ingresos);
                    //FIN PREPARAR 
                    
                    $queryAdicionales->bindParam(":domicilio",$domicilio);
                    $queryAdicionales->bindParam(":fecha_nacimiento",$fechaNacimiento);
                    $queryAdicionales->bindParam(":identificacion",$identificacion);
                    $queryAdicionales->bindParam(":comprobante_ingresos",$comprobanteIngreso);
                    $queryAdicionales->bindParam(":ingresos",$ingresos);
                    $queryAdicionales->execute();
                    
                    echo '<p>TU SOLICITUD HA SIDO PROCESADA CORRECTAMENTE SU NÚMERO DE REGISTRO ES: <strong>'.$candidatoId. '</strong>  EN BREVE NOS COMUNICAREMOS CON USTED. </p>';
                    
             }
             catch (Exception $ex) {
             
                 echo 'ERROR'. $ex->getMessage();
             }
          
         }
     }

    
}
