<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PanelModel
 *
 * @author Victor Ortiz
 */
require '../db/Conexion.php';
class PanelModel {
    private $conn;
    private $current;
    private $limit_l;
    private $limit_h;
    private $rows;
    
    private $sqlView="CREATE OR REPLACE VIEW candidato_full AS(SELECT candidato_web.candidato_id, candidato_web.nombre, candidato_web.apellido_paterno,
                  candidato_web.apellido_materno, candidato_web_datos_adicionales.telefono_fijo,
                  candidato_web_datos_adicionales.telefono_movil, candidato_web_datos_adicionales.calle, 
                  candidato_web_datos_adicionales.cp,
                  candidato_web_datos_adicionales.num_ext, 
                  candidato_web_datos_adicionales.num_int, 
                  candidato_web_datos_adicionales.colonia, 
                  candidato_web_datos_adicionales.estado, 
                  candidato_web_datos_adicionales.comprobante_domicilio, 
                  candidato_web_datos_adicionales.comprobante_ingresos, 
                  candidato_web_datos_adicionales.identificacion_vigente,
                  candidato_web_datos_adicionales.ingresos 
                  FROM candidato_web INNER JOIN candidato_web_datos_adicionales 
                  ON candidato_web.candidato_id=candidato_web_datos_adicionales.candidato_id)"; 
    
    
    public function __construct() {
         $conexion=new Conexion();
         $this->conn=$conexion->getConexion(); 
         $this->crearView();         
         $this->current=1;
         $this->rows=25;
         $this->limit_l=($this->current * $this->rows) - ($this->rows);
         $this->limit_h=$this->limit_l + $this->rows; 
         
    }
     public  function crearView(){
         $query=$this->conn->prepare($this->sqlView);
         $query->execute();
     }
     public function getCandidatos($request){
       
          $resultados;
          
          if (isset($request['rowCount'])){
            $this->rows=$request['rowCount'];
          }
          //calculate the low and high limits for the SQL LIMIT x,y clause
         if(isset($request['current']) )
          {
                $this->current=$request['current'];
                $this->limit_l=($this->current * $this->rows) - ($this->rows);
                $this->limit_h=$this->rows ;
          }
          if ($this->rows==-1)
                $limit=""; //no limit
          else{
              $limit="LIMIT ".$this->limit_l.",".$this->limit_h;
          }
          if(isset($request['searchPhrase'])){
                    
             $busquedaStr= trim($request["searchPhrase"]);
             $resultados=$this->ejecutarConsulta("SELECT*FROM candidato_full WHERE nombre LIKE '".$busquedaStr."%' 
             OR apellido_paterno LIKE '".$busquedaStr."%' OR apellido_materno LIKE '".$busquedaStr."%' OR candidato_id
             LIKE '".$busquedaStr."%' OR telefono_fijo LIKE'".$busquedaStr."%' OR telefono_movil LIKE'".$busquedaStr."%' $limit");
          }
          else{
                  $resultados=$this->ejecutarConsulta("SELECT*FROM candidato_full $limit");           
          }
          
         
          $queryCount=$this->conn->prepare("SELECT COUNT(*) FROM candidato_full");
          $queryCount->execute();
          $nRows=$queryCount->fetchColumn();      
          
          $rows=$this->rows;
          $current=$this->current;
          $jsonResult="{ \"current\":$current, \"rowCount\":$rows, \"rows\": ". json_encode($resultados).", \"total\": $nRows }";
   
         return $jsonResult;
     
              
     }
     
     private function ejecutarConsulta($sqlQuery){
          $query=$this->conn->prepare($sqlQuery);
          $query->execute();
          $resultados=$query->fetchAll(PDO::FETCH_ASSOC);
          return $resultados;
     }
     
  
     
     public function toCsv(){
                                                     
           $query=$this->conn->prepare($this->sqlQuery);
           $query->execute();
           // Output array into CSV file
           $filename="reporte.csv";
           $fp = fopen('php://output', 'w');
           $encabezados=["ID","NOMBRE","PATERNO","MATERNO","TELEFONO",
           "TELEFONO MOVIL","CALLE","CP","NUM_EXT","NUM_INT","COLONIA","ESTADO","COMPROBANTE DOMICILIO",
           "COMPROBANTE INGRESOS","IDENTIFICACION","INGESOS"];
           
           header('Content-Type: text/csv');
           header('Content-Disposition: attachment; filename="'.$filename.'"');
           fputcsv($fp, $encabezados);
           while ($row = $query->fetch(PDO::FETCH_ASSOC)) {   
               
                fputcsv($fp, $row);
           }
           fclose($fp);
     
     }
    
}
