<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of candidato
 *
 * @author Victor Ortiz
 */
class candidato {
  private $candidatoId;
  private $nombre;
  private $paterno;
  private $materno;
  private $telefono;
  private $contato;
  private $contacto_fecha;
  private $interesado;
  private $ultimo_intento;
  private $contacto_candidatoLlamada;
 
  public function getInteresado(){
      return $this->interesado;
  }
  public function setInteresado($interesado){
      $this->interesado=$interesado;
  }
  public function getCandidatoId(){
      return $this->candidatoId;
  }
  public function setCandidatoId($candidatoId){
      $this->candidatoId=$candidatoId;
  }
  public function getNombre(){
      return $this->nombre;
  }
  public function setNombre($nombre){
      $this->nombre=$nombre;
  }
  public function getPaterno(){
      return $this->paterno;
  }
  public function setPaterno($paterno){
      $this->paterno=$paterno;
  }
  
   public function getMaterno(){
      return $this->materno;
  }
  public function setMaterno($materno){
      $this->materno=$materno;
  }
    public function setTelefono($telefono){
      $this->telefono=$telefono;
  }
  public function getTelefono(){
      return $this->telefono;
  }
  public function setContacto($contacto){
      $this->contacto=$contacto;
  }
  public function getContacto(){
      return $this->contato;
  }
  
  public function setContactoFecha($contactoFecha){
      $this->contacto_fecha=$contactoFecha;
  }
  public function getContactoFecha(){
      return $this->contacto_fecha;
  }
  
  public function getUltimoIntento(){
      return $this->ultimo_intento;
  }
  public function setUltimoIntento($intento){
      $this->ultimo_intento=$intento;
  }
  public  function getContactoCandidatoLlamada(){
      return $this->contacto_candidatoLlamada;
      
  }
  public function setContactoCandidatoLlamada($candidato){
      $this->contacto_candidatoLlamada=$candidato;
  }
}
