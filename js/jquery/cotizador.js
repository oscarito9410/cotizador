$(document).ready(function() 
{
        new WOW().init();
        var $pago,$saldo,$monto,$plazo,$periodicidad,$row;
        $("#montoLbl" ).text("0");
        $("#goRegistro").click(function(e){
            
                e.preventDefault();
                guardarStorage();
                $('body').fadeOut(300, function(){
                    document.location.href ="registro.html";
                });
        });
        //Slider Monto
        $("#sliderMonto").slider({
              range: "min",
              animate: true,
              value:100,
              min: 100,
              max: 10000,
              step: 100,
              slide: function(event, ui) {
                //$("#btnHelpMonto").animateCSS("swing");  
                $monto=ui.value;
                update(); //actualizamos la vista
              }
              
          });
        //Slider Plazo
        $("#sliderPlazo").slider({
              range: "min",
              animate: true,
              value:1,
              min: 1,
              max: 12,
              step: 1,
              slide: function(event, ui) {
                   $plazo=ui.value;  
                   update(); //actualizamos la vista
               }
          });
        //Cambio selección periodicidad
        $("#periodicidad").on("change",function(){
           //Modificar maximo de slider Plazo de acuerdo a la opción seleccionada
            var  option=parseInt($("#periodicidad").val());
            switch(option){
                case 1://Mes
                     $("#sliderPlazo").slider("option","max",12);
                     $("#tipoPlazo").text("mensulamente");
                    break;
                case 2://Quincena
                    $("#sliderPlazo").slider("option","max",25);
                     $("#tipoPlazo").text("quincenalmente");
                    break;
                case 3://Semanal
                    $("#sliderPlazo").slider("option","max",52);
                     $("#tipoPlazo").text("semanalmente");
                    break;
            }
           //Actualizamos la UI
           $plazo=5;
           $("#sliderPlazo").slider("value",5);
           update();

        });
       
      
        //Animación 0.5 segundo sespués
       setTimeout(function(){
                     //separador comillas del plugin
                     var separador=$.animateNumber.numberStepFactories.separator(',');
                     $monto=4000;
                     $plazo=5;
                     $saldo=0;
                     $("#sliderMonto").slider("value",$monto);
                     $("#sliderPlazo").slider("value",$plazo);
                     $( "#montoLbl" ).animateNumber(
                     {
                         number:$monto,
                         numberStep:separador
                     });

                     $("#pagoLbl").animateNumber({

                         number:(getFirstPago()),
                         numberStep:separador
                     });
                     guardarStorage();
                     cambiarTooltip();
         },100);
       function update() {
              $pago =getPago();
              $("#montoLbl" ).text(commaSeparador($monto));
              $("#pago").text($pago);
              $("#pagoLbl").text(commaSeparador($pago));
              cambiarTooltip();
       }
       function guardarStorage()
       {
              localStorage.setItem("monto",$monto);
              localStorage.setItem("pago",$saldo);
              localStorage.setItem("total",$pago);
              localStorage.setItem("plazo",$plazo);
              localStorage.setItem("periodo",$("#periodicidad option:selected").text());
       }
       function getPago(){
         $periodicidad=$("#periodicidad").val();
         var c = new Calculadora($monto,$saldo,$plazo,$periodicidad);
         return c.getPago();  
       }
       function getFirstPago(){
          var c = new Calculadora($monto,$saldo,2,1);
         return c.getPago(); 
       }
       function commaSeparador(n){
          return n.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
       }
       function cambiarTooltip(){
          $('#sliderMonto a').html('<label><span class="glyphicon glyphicon-chevron-left"></span> '+$monto+' <span class="glyphicon glyphicon-chevron-right"></span></label>');
          $('#sliderPlazo a').html('<label><span class="glyphicon glyphicon-chevron-left"></span> '+$plazo+' <span class="glyphicon glyphicon-chevron-right"></span></label>');
       }
       var sliderCount=0;
       setInterval(function(){
                   var $msj=$("#mensaje-slider");
                   $msj.fadeOut("slow",function(){

                        $("#texto-slider").text(sliderCount%2==0?"BUSCAMOS APOYARTE EN TU GIRO": "RÁPIDA RESPUESTA EN MENOS DE 5HRS");
                        sliderCount++;
                   });
                   $msj.fadeIn("slow");
           },8000);
       $('#modalPasos').on('show.bs.modal', function (event) {
               var button = $(event.relatedTarget);
               var tipo=button.data("tipo");

               if(tipo=="monto"){
                   $row=$("#row-monto");
                   $row.animateCSS("fadeIn");
               }
               else if(tipo=="pago"){
                   $row=$("#row-pago");
                   $row.animateCSS("fadeIn");
               }
               else if(tipo=="periodo"){
                   $row=$("#row-periodo");
                   $row.animateCSS("fadeIn");
               }

               else if(tipo=="plazo"){
                   $row=$("#row-plazo");
                   $row.animateCSS("fadeIn");
               }

           });
       $("#modalPasos").on('hidden.bs.modal', function () {
                 $row.css("display","none");
           });
       
  });

