   $(document).ready(function(){
             //Inicia el grid
             $("#grid").bootgrid();
             //Agregamos el boton descargar reporte
             $(".actionBar div:first").before('<div class="search form-group"><button id="descarga"   class="btn btn-block btn-lg btn-warning">Descargar reporte <span class="glyphicon glyphicon-cloud-download"></span></button> </div>');
             //Cambiamos el placeHolder
             $(".search-field").attr("placeholder","Buscar");
             //Descargar el csv
             $("#descarga").click(function(){
                 document.location.href="php/api/export.php";

             });
 });

