// Wrap IIFE around your code
(function($, viewport){
    $(document).ready(function() {
        $("#cotizador").css("visibility","visible");
        $(window).resize(
            viewport.changed(function() 
            {
                    change();
            })
        );

    });
    
    function change(){
         var lastClass='';
                 var ancho=$(window).width();
                
                 if(!empty(lastClass)){
                    $("#cotizador").removeClass(lastClass);
                 }
               
                if(viewport.is('md')) {
                 if(ancho<=1024 && ancho<=1400){
                    lastClass="col-md-6";
                    $("#cotizador").addClass(lastClass);
                }
                else if(ancho>=1400 && ancho<=1600){
                     lastClass="col-md-5";
                    $("#cotizador").addClass(lastClass);
                }
                else  if(ancho>1600){
                     lastClass="col-md-4";
                    $("#cotizador").addClass(lastClass);
                }
                
                }
                
                 
                
                     
            $("#cotizador").css("visibility","visible");
    }
    function empty(str){
            return !str || !/[^\s]+/.test(str);
        }
})(jQuery, ResponsiveBootstrapToolkit);


