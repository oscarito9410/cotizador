app.controller('modalControlador', function ($scope,$log,$http,$uibModalInstance,apiService) {
  $scope.formModal={};
  $scope.submitModal = function () {
      //El formulario es invalidado 
     if(!$scope.formModalPop.$valid){
         return;
     }
    
     $scope.formModal.from_modal=true;
     console.log(JSON.stringify($scope.formModal));
     $scope.data=JSON.stringify($scope.formModal),
     apiService.enviarCandidato($scope.data);
     
     $uibModalInstance.dismiss('cancel');
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});