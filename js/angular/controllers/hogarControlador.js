app.controller("hogarControlador",function($scope,apiService,sectionService,formService){ 
    $scope.formService=formService;
    
    //OPCIONES POR DEFAULT DE LOS FORMULARIOS
    formService.formHogar.comprobante_ingresos=false;
    formService.formHogar.identificacion=false;
    formService.formHogar.domicilio=false;
    formService.formHogar.ingresos=0;
   
    $scope.codigos=new Array();
    $scope.colonias=[];
    $scope.delegaciones={};
  
    
    apiService.getCodigos().then(function(response){
           for(var i=0;i<response.data.length;i++){
                             $scope.codigos.push(response.data[i].cp);
          }
    });
    
    $scope.modelOptions = {
        debounce: {
          blur: 250
        },
        getterSetter: true
      };
    
    apiService.getDelegaciones().then(function(response){
         $scope.delegaciones=response.data;
    });

    $scope.ngModelOptionsSelected = function(value) {
      if (arguments.length){
            formService.formHogar.cp=value;
            $scope.cp_invalido=false;
            apiService.getData(formService.formHogar.cp).then(function(response){
             
                            if(response.data.length>0)
                            {
                                $scope.cp_invalido=false;
                                $scope.colonias.length=0;
                                 for(var i=0;i<response.data.length;i++){
                                        $scope.colonias[i]=response.data[i].colonia;
                                 }
                                formService.formHogar.colonia=response.data[0].colonia;
                                formService.formHogar.delegacion=response.data[0].delegacion;
                            }
                        
                          });
       } 
       else {
        return formService.formHogar.cp;
      }
    };
   $scope.cp_invalido=false;
   $scope.exitsCp=function(cp){
    if(cp.length>=4){
        if($scope.codigos.indexOf(cp)===-1){ 
             $scope.cp_invalido=true;
        }
        else{
            $scope.cp_invalido=false;
        }
    }
    else{
        $scope.cp_invalido=false;
    }
   };
   
    apiService.getColonias().then(function(response){
        if(response.data.length>0)
        {
            $scope.colonias.length=0;
            for(var i=0;i<response.data.length;i++){
                 $scope.colonias[i]=response.data[i].colonia;
            }
            formService.formHogar.colonia=$scope.colonias[0];
      }
    
   });
   
  
   
    $scope.submitHogar=function(){
          
           if($scope.string2Bool(formService.formHogar.domicilio)===false){
                swal({ title: "Atención!",text:"Es necesario contar con un comprobante  de domicilio vigente",   
                        type:"warning"}); 
            
           }
           else if($scope.string2Bool(formService.formHogar.identificacion)===false){
               swal({ title: "Atención!",text:"Es necesario contar con identifiación oficial vigente",   
                        type:"warning"});
           }
           else if($scope.string2Bool(formService.formHogar.comprobante_ingresos)===false){
                 swal({ title: "Atención!",text:"Es necesario contar con comprobante de ingresos",   
                        type:"warning"});
           }
           else
           {
                //Antes de mostrar el nuevo formulario modificacimos los campos 
                //Domicilio, Identificación y Comprobante de ingresos
                formService.formHogar.domicilio=true;
                formService.formHogar.identificacion=true;
                formService.formHogar.comprobante_ingresos=true;
                //Mostramos el nuevo formulario
                angular.element(document.querySelector("#formHogar")).css("display","none");
                angular.element(document.querySelector("#formTrabajo")).show();
                $scope.sectionService.showTrabajo=true;
            }
    };
 
   $scope.string2Bool=function(str){
      return (str==="true");
   };
    
});
