app.controller('formControlador', function ($scope,$timeout,$uibModal,sectionService,apiService,formService){
   //SERVICIO SECCIÓN
    sectionService.initVariables();
    sectionService.getLocalStorage();
    formService.initService();
    $scope.sectionService=sectionService;
    $scope.formService=formService;
    //VARIABLE PARA VERIFICAR SI LA FECHA ES VALIDA O NO
    $scope.noValidateDate=false;
   
    //ANIMACIONES OPCIONES
    $scope.showLoader=true;
    $scope.showRegistro=false;
    $scope.showAlert=false;
    //JSON USE FOR TEST
    $scope.jsonPrueba='{"nombre_ref1":"121212","telefono_ref1":"121212","nombre_ref2":"121212","telefono_ref2":"1212","nombre_empresa":"2121","giro_empresa":"211212","referencia_empresa":"121212","ref1Tel_empresa":"121212","referencia2_empresa":"121212","ref2Tel_empresa":"121212","estado":"Guanajuato","delegacion":"Alvaro Obregon","colonia":"121212","cp":"12122","calle":"121221","interior":"12121","exterior":"21121","domicilio":true,"identificacion":true,"comprobante_ingresos":true,"ingresos":"121212","nombre":"OSCAR","paterno":"PEREZ","materno":"MARTINEZ","email":"oemy93@hotmail.com","edad":"20","sexo":"M","telFijo":"12122112","telMovil":"121212","fecha_nacimiento":"1997-02-09T06:00:00.000Z"}';
    //ANIMACION DE LOADER
    $scope.initLoader=function(){
        $timeout(function(){
            $scope.showLoader=false;
            $scope.showRegistro=true;
            $timeout(function(){
                 $scope.showAlert=true;
                
            },500);
        },2000);
        
             
    };

     $scope.dynamicPopover = {
        templateUrl: 'myPopoverTemplate.html',
        title: 'Comprobantes de ingresos '
  };

    $scope.submitTrabajo=function(){
                angular.element(document.querySelector("#formTrabajo")).css("display","none");
                angular.element(document.querySelector("#formReferencia")).show();
                $scope.sectionService.showReferencias=true;
    };
    $scope.submitReferencia=function(){
                angular.element(document.querySelector("#formReferencia")).css("display","none");
                angular.element(document.querySelector("#formPrivacidad")).show();
                $scope.sectionService.showPrivacidad=true;
    };
    $scope.submitPrivacidad=function(){
                //Preparando los datos para mandarlos al servidor
                //concatenamos los arrays de los formularios
                $scope.finalData=angular.extend(formService.formHogar,formService.formPersonal);
                $scope.finalData=angular.extend(formService.formTrabajo,$scope.finalData);
                $scope.finalData=angular.extend(formService.formReferencia,$scope.finalData);
                //Hacemos post request
               console.log("JSON RESULT FORMS");
               console.log(JSON.stringify($scope.finalData));
               apiService.enviarCandidato(JSON.stringify($scope.finalData));
                
    };
      
   /*
    * MODAL FUNCIONES
    */
    $scope.open = function (size) {
    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: 'html/modalNombre.html',
      controller: 'modalControlador',
      size: size,
      resolve: {
        items: function () {
          return $scope.items;
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
      $scope.selected = selectedItem;
    }, function () {
      
    });
  }; 
     
});
