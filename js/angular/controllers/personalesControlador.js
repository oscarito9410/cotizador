app.controller("personalesControlador",function($scope,sectionService,formService){
      
    $scope.formService=formService;
    $scope.sectionService=sectionService;
    /*
     * FORM PERSONAL
     * VALIDACIONES 
     */   
    //Selcción de genero
    $scope.selectGeneroM=function(){
      
       formService.formPersonal.sexo='M';
    };
    $scope.selectGeneroF=function(){
        formService.formPersonal.sexo='F';
    };
    //FUNCION VALIDAR DATE
    $scope.validarDate=function(){
        
              $scope.valida= moment($scope.anio+"-"+$scope.mes+"-"+$scope.dia,'YYYY-MM-DD');
              
              if(isNaN($scope.anio)|| isNaN($scope.anio)|| isNaN($scope.dia)){
                  $scope.noValidateDate=true;
                  return;
              }
              
              if($scope.valida.isValid()){
                   $scope.noValidateDate=false;
                   formService.formPersonal.edad=moment().diff($scope.valida,'years');
                                                  
              }
              else{
                  $scope.noValidateDate=true;
              }
    };
    $scope.submitPersonales=function(){
            if(isNaN($scope.anio)|| isNaN($scope.mes)|| isNaN($scope.dia)){
                alert("Fecha no valida");
                return;
            }
            //Fecha usuando  ISO formato yyyy/MM/dd
            formService.formPersonal.fecha_nacimiento=new Date($scope.anio+"/"+$scope.mes+"/"+$scope.dia);
            angular.element(document.querySelector("#formPersonales")).css("display","none");
            angular.element(document.querySelector("#formHogar")).show();
            $scope.sectionService.showHogar=true;
            formService.formHogar.calle="";
    };
    
});


