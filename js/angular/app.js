   var app=angular.module('registro', ["ngRoute","ngAnimate","ngMask","ui.bootstrap"]).config(function() {
        new WOW().init();
    }); 
    
    
    app.service("sectionService",function(){
        
        this.initVariables=function(){
            //Visibilidad secciones
             //Form datos personales
              this.showPersonal = true;
             //Form datos hogar
              this.showHogar=false;
             //Form datos trabajo
              this.showTrabajo=false;
             //Form referencias
              this.showReferencias=false;
             //Form privacidad
             this.showPrivacidad=false;
        };
        
        this.getLocalStorage=function(){
            //Obtenemos datos de la página anterior
            this.pago=localStorage.getItem("pago");
            this.monto=localStorage.getItem("monto");
            this.total=localStorage.getItem("total");
            this.plazo=localStorage.getItem("plazo");
            this.periodo=localStorage.getItem("periodo");
            
        };
        
    });
    app.service("formService",function(){
      this.initService=function(){  
            this.formPersonal={};
            this.formTrabajo={};
            this.formHogar={};
            this.formReferencia={};
            this.formPrivacidad={};
        };  
      
    });
    app.service("apiService",function($http){
             
                
                this.enviarCandidato=function(json){
             
                    $http({
                            method: 'POST',
                            url: "php/api/metodos.php",
                            data:json,
                            headers: {'Content-type': 'application/json'}
                        }).success(function (data, status, headers, config) {
                           swal({   
                                title: "SOLICITUD CRÉDITO",
                                text:data,   
                                html: true,
                                type:"success"});
                        }).error(function (data, status, headers, config) {
                            document.write(data);
                        });
                };
                this.getColonias=function(){
                    var data=new FormData()
                    data.append("tipo","getColonias");
                    var promise=$http({
                        method:'POST',
                        url:"php/api/metodos.php",
                        data:data,
                        headers:{'Content-Type':undefined}
                    }).success(function(data,status,headers,config){
                        return data;
                    }).error(function(data,status,headers,config){
                        return data;
                    });
                    return promise;
                }
                this.getDelegaciones=function(){
                    var data=new FormData();
                    data.append("tipo","getDelegaciones");
                    var promise=$http({
                        method:'POST',
                        url:"php/api/metodos.php",
                        data:data,
                        headers: { 'Content-Type': undefined }
                    }).success(function(data,status, headers, config){   
                        return data;
                   });
                   return promise;
                }
                this.getCodigos=function()
                {
                    var data= new FormData();
                    data.append("tipo","getCodigos");
                    var promise=$http({
                       method:'POST',
                       url:"php/api/metodos.php",
                       data:data,
                       headers: { 'Content-Type': undefined }
                      
                     }).success(function(data,status, headers, config){   
                        return data;
                    }).error(function(data){
                        
                    })
                     return promise; 
                };
                
                this.getData=function(cp){
                    var data=new FormData();
                    data.append("tipo","getData");
                    data.append("cp",cp);     
                    var promise=$http({
                        method:'POST',
                        url:"php/api/metodos.php",
                        data:data,
                        headers:{'Content-Type':undefined}
                    }).success(function(data,status,headers,config){
                         console.log("Data"+data);
                         return data;
                    }).error(function(data,status,headers,cofig){
                        return data;
                    });
                    return promise;
                };
                
               
               
            
    });
  
   app.directive("googleMaps",function(){
        
        return{
            restrict:'A', //atributo
            /*
             * "@"   (  Text binding / one-way binding )
               "="   ( Direct model binding / two-way binding )
               "&"   ( Behaviour binding / Method binding  )
             */
            link:function(scope,element,attrs,model){
                $(element).locationpicker({
                    location: {latitude:19.361014, longitude:-99.151348},
                    radius: 20,
                    zoom:20,
                    locationName:'',
                    inputBinding: {
                        locationNameInput: $("#calle"),
                        latitudeInput:$("#laltitud"),
                        longitudeInput:$("#longitud")
                    },
                    enableAutocomplete: true,
                    oninitialized:function(){
                        var map = $(element).locationpicker('map').map;
                        google.maps.event.addListener(map, "idle", function(){
                            google.maps.event.trigger(map, 'resize'); 
                        });
                  },
                   
                });
                
              
            }
        };
        
    });
   app.directive("soloLetras",function($timeout){    
       return{
           restrict:'A',
          link:function(scope,element,attrs,model){
           element.bind("keyup",function(){
                  var text=$(element).val();
                  var re = /^[A-Za-z]+$/;
              
                  if(!re.test(text)){
                        $(element).val("");
                    }
                   
            });      
           
          
           }
      };
   });
   app.directive('soloNumeros', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }            
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});